console.log("-----------------------------Pagina 100---------------------------");
console.log( String.fromCharCode(0x61) );
console.log( String.fromCharCode(0x61) + String.fromCharCode(0x61) );
console.log( String.fromCharCode(0x68, 0x65, 0x6C, 0x6C, 0x6F) );
console.log( String.fromCharCode(119, 111, 114, 108, 100) );
console.log( String.fromCharCode('0x61') ); // note this is a String of a hexadecimal
console.log( String.fromCharCode(0xD83D, 0xDCD6) ); // Open Book Emoticon
console.log( String.fromCharCode(0x10102) ); // Number > 16 bits
console.log( 'a'.charCodeAt(0) ); // 97
console.log( 'aa'.charCodeAt(1) ); // 97
console.log( 'hello'.charCodeAt(4) ); // 111
console.log( 'abc'.charCodeAt(4) ); // NaN
console.log( '\uD83D\uDCD6'.charCodeAt(0) ); // 55357
console.log( '\uD83D\uDCD6'.charCodeAt(1) ); // 56534
console.log( '\u{1F4D6}'.charCodeAt(1) ); // 56534    