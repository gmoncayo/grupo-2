console.log("-----------------------------Pagina 95---------------------------");   
console.log( '"Hola" tiene ' + ( 'Hola'.length ) + ' de longitud' );
console.log( '"1234" tiene ' + ( '1234'.length ) + ' de longitud' );
console.log( '"0061" tiene ' + ( '0061'.length ) + ' de longitud' );
console.log( '"\u0061" tiene ' + ( '\u0061'.length ) + ' de longitud' );
console.log( '"length" tiene ' + ( 'length'.length ) + ' de longitud' );
console.log( '"Mixed\u0055nicode" tiene ' + ( 'Mixed\u0055nicode'.length ) + ' de longitud' );
console.log( '"\uD83D\uDCD6" tiene ' + ( '\uD83D\uDCD6'.length ) + ' de longitud' );