class CheckArrayInstance {
    static [Symbol.hasInstance](instance){
        return Array.isArray(instance)
        }
}
var myArray = new Array();
console.log(myArray instanceof CheckArrayInstance); //returns true