

    console.log("-----------------------------Ejercicio Página 47---------------------------");

    var name = 'Bob';
    console.log( 'Hello ' + name + '!' ); // The old way to do this, cumbersome and error prone
    console.log( `Hello ${name}!` ); // Template Literals, shorter, more succinct, and less
    var otherName = 'Mary';
    var thirdName = 'Jim';
    console.log( 'Hello ' + otherName + ', how is ' + thirdName + '?' ); // Can get very messy
    console.log( `Hello ${otherName}, how is ${thirdName}?` );

