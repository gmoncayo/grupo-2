
    console.log("-----------------------------Ejercicio Página 5---------------------------");
    
    function foo3() {
        var a = 1;
        bar();
        function bar() {
            var b = 2;
            baz();
            function baz() {
                console.log("Valores de funciones bar() y baz() creadas dentro de la funcion foo3()");
                console.log(a, b);
            }
        }
    }
    function bing() {
        var a = 1;
        boo();
        function boo() {
            console.log("Valor de la funcion boo() creada dentro de bing()");
            console.log(a);
        }
    }
    foo3();
    bing();
