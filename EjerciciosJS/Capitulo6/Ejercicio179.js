
    console.log("-----------------------------Página 179---------------------------");

     //Want to know what the offset between UTC time and my local time
     var currentDate = new Date();
     var offSet = currentDate.getTimezoneOffset() // 60; converts minutes to hours
     console.log(offSet);
     //check if we are on daylight savings time
     var today = new Date();
     function isDST(){
         var jan = new Date(today.getFullYear(), 0, 1);
         var jul = new Date(today.getFullYear(), 6, 1);
         return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
     }
     if(isDST() != today.getTimezoneOffset()){
         console.log('On DST');
     }else{
         console.log('Not On DST');
     }
    //format the date using toLocaleTimeString
    console.log(currentDate.toLocaleDateString('ja-JP'));
    console.log(currentDate.toLocaleDateString('ja-JP', {weekday: 'long', year:'numeric',
    month:'short', day:'numeric', hour:'2-digit', minute:'2-digit'}))
    new Intl.DateTimeFormat('ja-JP', { weekday: 'long', year:'numeric', month:'short',
    day:'numeric', hour:'2-digit', minute:'2-digit', timeZone:'Asia/Tokyo', timeZoneName:
    'short'}).format(new Date(1975, 07, 19)); //returns 1975年8月19日火曜日 13:00 JST
    new Intl.DateTimeFormat('en-US', { weekday: 'long', year: 'numeric', month: 'long', day:
    'numeric' }).format(new Date(1975, 07, 19)); // returns Tuesday, August 19, 1975

