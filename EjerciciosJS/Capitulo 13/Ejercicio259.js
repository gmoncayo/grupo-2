function *insideThrow(){
    while(true){
        try{
            yield 'inside try block';
               }catch(e){
                   console.log('inside catch');
               }
    }
}
var it = insideThrow();
console.log(it.next());
console.log(it.throw(newError('this is an error')));
//value Object {value: "inside try block", done: false}
function * outsideThrow(){
    var value = yield value;
    return value;
}

var it2 = outsideThrow();
console.log(it2.next());
try{
    console.log(it2.next('outside try block'));
    done: true}
 //   console.log(it2.throw(new Error('this is an error')));
catch(e){
    console.log('outside catch');
}