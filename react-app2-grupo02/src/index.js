
import React from 'react';
import StyleSheet from 'react';
import ReactDOM from 'react-dom';
import './index.css';

//Hijo
// function Square(props){
    
//     return(
//         <button className="square" onClick={props.onClick} color="#ff5c5c">
//             {props.value}
//         </button>
//     );
// }

//const styles = StyleSheet.create({
//  cambiar:{
//    color: "#ff5c5c"
//  }
//});


const changeCol = document.getElementById("square");

//changeCol.forEach(element => {
//  element.textContent = 'background';
//});
//changeCol[0];

//const isBackgroundRed = true;

//function ColorSquare(){
//  changeCol.background = "#ffff00";
//}

class Square extends React.Component {
  //constructor(props){
  //  super(props);
  //  this.state = {cambiarColor: false};
  //}
  render() {
    return (
      //<view style={styles.cambiar}>
      <button
        //className={ props.winner ? 'squareWinner' : 'square' }
        className='square' 
        onClick={() => this.props.onClick()}
        color="#ff5c5c"
        backgroundColor = "#00ff00"
      >
        {this.props.value}
      </button>
      //</view>
    );
  }
}

  //Padre
class Board extends React.Component {
    constructor(props){
        super(props);
        
        this.state={
            squares: Array(9).fill(null),
            xisNext: true,
            indice: Number,
            backgroundColor: null,
            changeCol: document.getElementById("square"),
        };

    }

    handleClick(i){
        
      //const history = this.state.history.slice(0, this.state.stepNumber + 1);
      //const current = history[history.length - 1];
      const squares = this.state.squares.slice();
        //console.log('timbrao:  '+i);
        //this.state.indice = i;
        this.setState({
            indice: i            
        });
        if(calculateWinner(squares) || squares[i]){
            return;
        }
        squares[i]=this.state.xisNext ? 'X' : 'O';
        this.setState({
            squares: squares,
            xisNext: !this.state.xisNext,
        });
        

    }

    renderSquare(i) {
      return (
      <Square 
        value={this.state.squares[i]} 
        onClick={() => this.handleClick(i)} 
        //winner={this.isWinnerSquare(i)}
        color="#5cff5c"
        //background={"#00ff00"}
        />
        
      );
    }

    posicionFilaColumna(i){
      let fila_columna;
      switch (i) {
        case 0:
            fila_columna = "Fila: 0 & Columna: 0"               
            break;
        case 1:
            fila_columna = "Fila: 0 & Columna: 1"               
            break;
        case 2:
            fila_columna = "Fila: 0 & Columna: 2"               
            break;
        case 3:
            fila_columna = "Fila: 1 & Columna: 0"               
            break;
        case 4:
            fila_columna = "Fila: 1 & Columna: 1"               
            break;
        case 5:
            fila_columna = "Fila: 1 & Columna: 2"               
            break;
        case 6: 
            fila_columna = "Fila: 2 & Columna: 0"   
            break;
        case 7: 
            fila_columna = "Fila: 2 & Columna: 1" 
            break;
        case 8:
            fila_columna = "Fila: 2 & Columna: 2"              
            break;
        
        default:
            break;
      }
      return fila_columna;
    }
    
    render() {
      const winner = calculateWinner(this.state.squares);
      //const current = history[this.state.stepNumber];
      //const winner = caculateWinner(current.squares);
      let status;

      if(winner){
          status = 'Ganador: ' + winner;
      }else{
        status = 'Le toca ha: ' + (this.state.xisNext ? 'X' : 'O');
      }
      
      return (
          

        <div>
          <div className="status">{status}</div>
          <div className="status">{this.posicionFilaColumna(this.state.indice)}</div>

          <div className="board-row">
            {this.renderSquare(0)}
            {this.renderSquare(1)}
            {this.renderSquare(2)}

          </div>
          <div className="board-row">
            {this.renderSquare(3)}
            {this.renderSquare(4)}
            {this.renderSquare(5)}
          </div>
          <div className="board-row">
            {this.renderSquare(6)}
            {this.renderSquare(7)}
            {this.renderSquare(8)}
          </div>
        </div>
    
      );
    }
}
  
  class Game extends React.Component {
    render() {
      return (
        <div className="game">
          <div className="game-board">
            <Board 
            //squares = {current.squares}
            //winner = {winner}
            onClick = {(i) => this.handleClick(i)}/>
          </div>
          <div className="game-info">
            <div>{/* status */}</div>
            <ol>{/* TODO */}</ol>
          </div>
        </div>
      );
    }
  }
  
  function calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    //document.getElementById("square").background = "ffff00";
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        //background = "00ff00"
        //document.getElementById('square').style.background="00ff00";
        //ColorSquareWinner();
        //changeCol[a]= "00ff00";
        //changeCol.setAttribute('background', '00ff00');
        //isBackgroundRed = false;
        //<div className="square">
        //    {this[i].background("#00ff00")}
        //  </div>
        return (squares[a]);
      }
    }
    return null;
  }
  // ========================================
  
  ReactDOM.render(
    <Game />,
    document.getElementById('root')
  );
  