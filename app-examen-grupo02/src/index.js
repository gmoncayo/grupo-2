import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import firebaseConfig from './firebase-config.js'
import './firebase'
import 'bootswatch/dist/superhero/bootstrap.min.css'
import {FirebaseAppProvider} from 'reactfire'

ReactDOM.render(
  <FirebaseAppProvider firebaseConfig={firebaseConfig}>
    <Suspense fallback={'Abriendo la App del grupo 2. PROGRAMACIÓN WEB. UCE...'}>
      <App />
    </Suspense>    
  </FirebaseAppProvider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
