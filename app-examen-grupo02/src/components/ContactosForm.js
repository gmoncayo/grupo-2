import React, {useState, useEffect} from 'react';
import { db } from '../firebase';
import { mockComponent } from 'react-dom/test-utils';

const ContactoForm = (props )=> {

    const initialStateValues={
        nombre:'',
        apellido:'',
        email:'',
        telefono:'',
        fecha_nacimiento:'',
        edad: '',
        likes: ''
    };
    const [values, setValues] = useState(initialStateValues);

    const handleInputChange = e => {
        const {name, value}  =e.target;
        setValues({...values, [name]: value})
        // ValidarNumeros();
    };

    // const validarEmail = str =>{
    //     return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3,4})+$/.test(str)
      
    // }
    
   

    const handleSubmit = e =>{
        e.preventDefault();

        values.edad=CalcularEdad();
        // console.log("values.edad"+ values.edad);
        // CalcularEdad();
        // ValidarNombre();
        // ValidarApellido();
        // ValidarTelefono();
        

        props.addOrEdit(values);
        setValues({...initialStateValues})
    }

    const getContactobyId = async (id) =>{
        const doc = await db.collection('Contactos').doc(id).get();
        setValues({...doc.data()})
    }

    useEffect(()=>{
        if(props.currentId === ''){
            setValues({...initialStateValues});
        }else{
            getContactobyId(props.currentId);
        }
    },[props.currentId])

    //----------------------------------------------
    function validarFecha ()
    {
        return /^(?:(?:(?:0?[1-9]|1\d|2[0-8])[/](?:0?[1-9]|1[0-2])|(?:29|30)[/](?:0?[13-9]|1[0-2])|31[/](?:0?[13578]|1[02]))[/](?:0{2,3}[1-9]|0{1,2}[1-9]\d|0?[1-9]\d{2}|[1-9]\d{3})|29[/]0?2[/](?:\d{1,2}(?:0[48]|[2468][048]|[13579][26])|(?:0?[48]|[13579][26]|[2468][048])00))$/.test(values.fecha_nacimiento)
    }

    function CalcularEdad(){
        if(validarFecha()){
            var anio = values.fecha_nacimiento.charAt(6) + values.fecha_nacimiento.charAt(7) + values.fecha_nacimiento.charAt(8) +values.fecha_nacimiento.charAt(9)

            var edad = parseInt(anio); 
            var resta = 2020 - edad;
            // console.log("su edad es: "+ resta);
            if(resta < 90){
                
                return resta;
            }else{
                values.edad = 0;
                return 'Error en la fecha';
            }
            
        }else{
            values.edad = 0;
            // console.log('Fecha incorrecta');
            return ;
        }
    }


    function ContadorLikes(){
        var contador = contador + 1;
    
    
        {
            return contador;
         
        }
    }



    function validarNumero(valor){
        if(valor != ''){
            return /^([0-9])*$/.test(valor);
        }else{
            return '';
        }
       
    }

    function validarNUmeroTelefono(){
        if(values.telefono.length <= 10 && values.telefono.length != 0){
            if(values.telefono.charAt(0) == 0 && values.telefono.charAt(1) == 9){
                return true;
            }else{
                return '';
            }
            
        }else{
            return '';
        }
        
    }

    function validarEmail(valor){
        return  /\S+@\S+\.\S+/.test(valor);
    }



    return (
        <form className="card card-body" onSubmit={handleSubmit}>

        <div className="form-group input-group"   > 
            <div className="input-group-text bg-light"> 
                <i className="material-icons">edit</i>
            </div>
            
        <input
            type="text" 

            className="form-control" 
            placeholder="Nombre" 
            name="nombre"
            onChange={handleInputChange}
            value={values.nombre}
            
            // onkeypress="return check(event)"
            
            />
        </div>

        <div className="form-group input-group"  >
            <div className="input-group-text bg-light">
                <i className="material-icons">edit</i>
            </div>
            <input 
            type="text" 
            className="form-control" 
            name="apellido" 
            placeholder="Apellido"
            onChange={handleInputChange}
            value={values.apellido}
            />
        </div>

        <div className="form-group input-group">
            <div className="input-group-text bg-light">
                <i className="material-icons">edit</i>
            </div>
            <input 
            type="text" 
            className="form-control" 
            name="email" 
            placeholder="Email"
            onChange={handleInputChange}
            value={values.email}
            />
        </div>

        <div className="form-group input-group">
            <div className="input-group-text bg-light">
                <i className="material-icons">edit</i>
            </div>
            <input 
            type="text" 
            className="form-control" 
            name="telefono" 
            placeholder="Telefono"
            onChange={handleInputChange}
            value={values.telefono}
            />
        </div>

        
        <div className="form-group input-group">
            <div className="input-group-text bg-light">
                <i className="material-icons">edit</i>
            </div>
            <input 
            type="text" 
            className="form-control" 
            name="fecha_nacimiento" 
            placeholder="dd/mm/aaaa"
            onChange={handleInputChange}
            value={values.fecha_nacimiento}
            />
        </div>

        
        <div className="form-group input-group">
            <div className="input-group-text bg-light">
                <i className="material-icons">edit</i>
            </div>
            <h1>{CalcularEdad()}</h1>
            
            {/* <input 
            type="text" 
            className="form-control" 
            name="edad" 
            placeholder="Edad"
            onChange={handleInputChange}
            value={values.edad}
            /> */}
        </div>

            
        
        <div className="form-group input-group">
            <div className="input-group-text bg-light">
                <i className="material-icons">edit</i>
            </div>
            <input 
            type="text" 
            className="form-control" 
            name="likes" 
            placeholder="likes"
            onChange={handleInputChange}
            value={values.fecha_nacimiento}
            />
        </div>

        <div className="form-group input-group">
            <div className="input-group-text bg-light">
                <i className="material-icons">edit</i>
            </div>
            <h1>
            {values.nombre!='' ? (validarNumero(values.nombre) ? ' Formato nombre: incorrecto -- ' : 'formato nombre: correcto -- ') : ''}
            {values.apellido!='' ? (validarNumero(values.apellido) ? ' Formato apellido: incorrecto -- ' : 'formato apellido: correcto -- ') : ''}
            {values.email!='' ? (validarEmail(values.email) ? ' Formato email: correcto -- ' : 'formato email: incorrecto -- ') : ''}
            {values.telefono!='' ? (validarNUmeroTelefono() ? ' Formato telefono: correcto-- ' : 'formato telefono: incorrecto, se guardará incorrectamente -- ') : ''}
            {/* {validarNUmeroTelefono()} */}
            {values.fecha_nacimiento!='' ? (validarFecha(values.fecha_nacimiento) ? ' Formato fecha: correcto -- ' : 'formato fecha: incorrecto -- ') : ''}
            {values.likes!='' ? (validarFecha(values.likes) ? ' Formato fecha: correcto -- ' : 'formato fecha: incorrecto -- ') : ''}
            
            </h1>
   
            
            {/* <input 
            type="text" 
            className="form-control" 
            name="edad" 
            placeholder="Edad"
            onChange={handleInputChange}
            value={values.edad}
            /> */}
        </div>

        <button className="btn btn-primary btn-block">
            {props.currentId === '' ? 'Guardar':'Actualizar'}
            {/* {validarNUmeroTelefono() ? this.button.disable= false : this.button.disable=true} */}

        </button>
        <div>
        <button className="btn btn-primary btn-block">
            {props.currentId === '' ? 'Likes':'Actualizar'}
          
        </button>
 

            
        <div>
        <button  onClick = {ContadorLikes}> ContadorLikes </button>
        </div>



        </div>
        
        </form>

    );

};

export default ContactoForm;

