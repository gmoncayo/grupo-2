import React, {useState} from 'react';
import 'firebase/auth';
import { useFirebaseApp, useUser } from 'reactfire';
import Contactos from './components/Contactos'

function LogOutSession(){
    const firebase = useFirebaseApp();
    const logout = async () =>{
        await firebase.auth().signOut();
    }
}

export default (props) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const firebase = useFirebaseApp();
    const user = useUser();
    
    const submit =  async () =>{
        await firebase.auth().createUserWithEmailAndPassword(email, password);
        console.log(email, password);
    }
    const logout = async () =>{
        await firebase.auth().signOut();
    }
    const login = async () =>{
        await firebase.auth().signInWithEmailAndPassword(email, password);
    }

    return(
        <div>
            {
                !user &&
                <div>
                    <label htmlFor="email">Correo electrónico</label>
                    <input type="email" id = "email" onChange= { (ev) => setEmail(ev.target.value)}/>
                    <label htmlFor="password"> Contraseña</label>
                    <input type = "password" id = "password" onChange= { (ev) => setPassword(ev.target.value)}/>
                    <button onClick = {login}>Iniciar Sesión</button>
                    <button onClick = {submit}>Crear Cuenta</button>
                </div> 
            }
            {
                user && 
                <div>
                    <label htmlFor="email">BIENVENIDOS AL GRUPO 2</label>
                    <br/>
                    <label htmlFor="A">INTEGRANTES: </label>
                    <br/>
                    <label htmlFor="B">FAUSTO CHACHA </label>
                    <br/>
                    <label htmlFor="C">FABIÁN PORTERO </label>
                    <br/>
                    <label htmlFor="D">JAROL SALTOS </label>
                    <br/><br/><br/><br/>
                    <Contactos/>
                    <button onClick = {logout}>Cerrar Sesión</button>
                </div>

            }   
        </div>
    )
}