import React from 'react';
import './App.css';
import Contactos from './components/Contactos'
import Auth from './Auth';
import {useFirebaseApp} from 'reactfire';

function App() {
  const firebase = useFirebaseApp();
  console.log(firebase);
  return (
    <div className="container p-4">
    {/* <dir className="row"> */}
      <Auth/> 
    {/* </dir>  */}
    </div>
  );
}


export default App;
