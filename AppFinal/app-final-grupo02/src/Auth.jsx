import React, {useState} from 'react';
import 'firebase/auth';
import { useFirebaseApp, useUser } from 'reactfire';
import Contactos from './components/Contactos'

export default (props) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState(' ');
    const firebase = useFirebaseApp();
    const user = useUser();
    const submit =  async () =>{
        await firebase.auth().createUserWithEmailAndPassword(email, password);
        console.log(email, password);
    }
    const logout = async () =>{
        await firebase.auth().signOut();
        setEmail('');
        setPassword('');
    }
    const login = async () =>{
        await firebase.auth().signInWithEmailAndPassword(email, password);
    }

    return(
        <div>
            {
                !user &&
                <div align = 'center'>
                    <label htmlFor="email">Correo electrónico</label>
                    <input type="email" id = "email" onChange= { (ev) => setEmail(ev.target.value)}/>
                    <label htmlFor="password"> Contraseña</label>
                    <input type = "password" id = "password" onChange= { (ev) => setPassword(ev.target.value)}/>
                    <button onClick = {login}>Iniciar Sesión</button>
                    <button onClick = {submit}>Crear Cuenta</button>
                </div> 
            }
            {
                user && 
                <div align = 'center'>
                    <label htmlFor="T1">UNIVERSIDAD CENTRAL DEL ECUADOR</label>
                    <br/>
                    <label htmlFor="T2">FACULTAD DE INGENIERÍA Y CIENCAS APLICADAS</label>
                    <br/>
                    <label htmlFor="email">INGENIERÍA EN COMPUTACIÓN GRÁFICA</label>
                    <br/>
                    <label htmlFor="email">OPTATIVA 3</label>
                    <br/>
                    <br/>
                    <label htmlFor="email">BIENVENIDOS AL GRUPO 2</label>
                    <br/>
                    <label htmlFor="A">Integrantes: </label>
                    <br/>
                    <label htmlFor="B">FAUSTO CHACHA </label>
                    <br/>
                    <label htmlFor="C">FABIÁN PORTERO </label>
                    <br/>
                    <label htmlFor="D">JAROL SALTOS </label>
                    <br/><br/>
                    <Contactos/>
                    <br/><br/>
                    <button onClick = {logout}>Cerrar Sesión</button>
                </div>

            }   
        </div>
    )
}