import React, {useEffect, useState} from 'react';
import ContactosForm from "./ContactosForm";
import {db} from '../firebase'

const Contactos = ()=> {

    const [contactos, setContactos]=useState([]);
    const [currentId, setCurrentId]=useState('');

    const addOrEdit = async (ContactoObject) =>{
        if(currentId === ''){
            await db.collection('Contactos').doc().set(ContactoObject)
        }else{
            await db.collection('Contactos').doc(currentId).update(ContactoObject);
            setCurrentId('');
        }
        
    }

    const onDeleteContacto= async id=>{
        if(window.confirm('Estas seguro de eliminar este contacto?')){
            await db.collection('Contactos').doc(id).delete();
            console.log('Contacto eliminado');
        }
    }

    const getContactos = async () => {
        db.collection("Contactos").onSnapshot((querySnapshot) => {
            const docs = [];
            querySnapshot.forEach((doc) => {
                docs.push({...doc.data(), id: doc.id});
            });
            setContactos(docs);         
        });
      
    }

    useEffect(() => {
        getContactos();
                
    }, []);

    return (
    <div>
    <div className="col-md-4">
        <ContactosForm {...{addOrEdit, currentId, contactos}}/>

    </div>
        
        <div className="col-md-8">
            {contactos.map(contacto => (
                <div className="card mb-1" key={contacto.id}>
                    <div className="card-body">
                    <div className="d-flex justify-content-between">
                        <h4>{contacto.nombre}</h4>
                       <div>
                       <button className="material-icons text-danger" 
                        onClick={()=>onDeleteContacto(contacto.id)}>Eliminar </button>
                        <button className='material-icons' onClick={() => setCurrentId(contacto.id)}>
                            Editar
                        </button>
                       </div>
                    </div>
                        <p>{contacto.telefono}</p>
                    </div>
                </div>
            ))}
        </div>

    </div>
    );
};

export default Contactos;