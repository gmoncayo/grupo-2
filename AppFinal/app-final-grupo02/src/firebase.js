
import firebase from 'firebase/app'
import 'firebase/firestore'

var firebaseConfig = {
  apiKey: "AIzaSyAV4x0CzFcEgz4JyZ2fz7ELPzc08zggWY0",
  authDomain: "app-final-grupo02-224c4.firebaseapp.com",
  databaseURL: "https://app-final-grupo02-224c4.firebaseio.com",
  projectId: "app-final-grupo02-224c4",
  storageBucket: "app-final-grupo02-224c4.appspot.com",
  messagingSenderId: "46871075209",
  appId: "1:46871075209:web:ba3f5c7a7b6403f317a958",
  measurementId: "G-Z9W95M7RBB"
  };
  // Initialize Firebase
  const fb = firebase.initializeApp(firebaseConfig);
  export const db = fb.firestore();