"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
console.log("-----------------------------Página 300---------------------------");
var rngError = new RangeError('The value is out of range');
console.log(rngError.message); //returns The value is out of range
var refError = new ReferenceError('This reference is not valid');
console.log(refError.message); //returns This reference is not valid
var myCustomError = /** @class */ (function (_super) {
    __extends(myCustomError, _super);
    function myCustomError(message) {
        return _super.call(this, message) || this;
    }
    return myCustomError;
}(Error));
var myCustomErrorInstance = new myCustomError('This is a Custom Error');
console.log(myCustomErrorInstance.message); //returns This is a Custom Error
console.log(myCustomErrorInstance.stack); //returns stack trace
try {
    throw new myCustomError('There has been a mistake');
}
catch (e) {
    console.log(e.message); //returns There has been a mistake
    console.log(e.stack); //returns stack trace
}
