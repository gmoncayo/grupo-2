console.log("-----------------------------Ejercicio Página 39---------------------------");
var date = new Date();
var error = new Error();
var blob = new Blob();
var object = new Object();
typeof date; // Returns "object"
typeof error; // Returns "object"
typeof blob; // Returns "object"
if (date instanceof Date) {
    console.log("date is a Date instance");
}
else if (typeof date == 'undefined') {
    console.log("date is an Error instance");
}
else if (typeof date == 'undefined') {
    console.log("date is a Blob instance");
}
else {
    console.log("date is an unknown instance");
}
if (error instanceof Date) {
    console.log("error is a Date instance");
}
else if (error instanceof Error) {
    console.log("error is an Error instance");
}
else if (typeof error == 'undefined') {
    console.log("error is a Blob instance");
}
else {
    console.log("error is an unknown instance");
}
if (blob instanceof Date) {
    console.log("blob is a Date instance");
}
else if (blob instanceof Error) {
    console.log("blob is an Error instance");
}
else if (blob instanceof Blob) {
    console.log("blob is a Blob instance");
}
else {
    console.log("blob is an unknown instance");
}
if (object instanceof Date) {
    console.log("object is a Date instance");
}
else if (object instanceof Error) {
    console.log("object is an Error instance");
}
else if (object instanceof Blob) {
    console.log("object is a Blob instance");
}
else {
    console.log("object is an unknown instance");
}
