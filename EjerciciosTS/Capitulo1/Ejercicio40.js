console.log("-----------------------------Ejercicio Página 40---------------------------");
var date = new Date();
var error = new Error();
var blob = new Blob();
var object = {};
if (date.constructor === Date) {
    console.log("date is a Date instance");
}
else if (date.constructor === Error) {
    console.log("date is an Error instance");
}
else if (date.constructor === Blob) {
    console.log("date is a Blob instance");
}
else {
    console.log("date is an unknown instance");
}
if (error.constructor === Date) {
    console.log("error is a Date instance");
}
else if (error.constructor === Error) {
    console.log("error is an Error instance");
}
else if (error.constructor === Blob) {
    console.log("error is a Blob instance");
}
else {
    console.log("error is an unknown instance");
}
if (blob.constructor === Date) {
    console.log("blob is a Date instance");
}
else if (blob.constructor === Error) {
    console.log("blob is an Error instance");
}
else if (blob.constructor === Blob) {
    console.log("blob is a Blob instance");
}
else {
    console.log("blob is an unknown instance");
}
if (object.constructor === Date) {
    console.log("object is a Date instance");
}
else if (object.constructor === Error) {
    console.log("object is an Error instance");
}
else if (object.constructor === Blob) {
    console.log("object is a Blob instance");
}
else {
    console.log("object is an unknown instance");
}
