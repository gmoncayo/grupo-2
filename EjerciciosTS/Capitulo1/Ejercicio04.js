console.log("-----------------------------Ejercicio Página 4---------------------------");
function foo() {
    var a = 3;
}
console.log("Primera parte, 'a' sin definir: " + a);
foo();
console.log("Segunda parte declarando 'a' en foo(): " + a);
var a = 3;
function foo1() {
    a = 4;
}
console.log("Variable 'a' declarada de forma global: " + a);
foo1();
console.log("Variable 'a' despues de haber declarado a=4 dentro de foo(): " + a);
var a = 3;
function foo2() {
    var a = 4;
}
console.log("Variable 'a': " + a);
foo2();
console.log(a);
