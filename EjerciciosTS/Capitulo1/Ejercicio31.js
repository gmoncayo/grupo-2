console.log("-----------------------------Ejercicio Página 31---------------------------");
var a_31 = NaN;
var b_31 = Infinity;
var c_31 = -Infinity;
var d_31 = 3;
if (isNaN(a_31)) {
    console.log(a_31);
}
else {
    console.log("a is a real Number, not NaN");
}
if (isFinite(b_31)) {
    console.log(b_31);
}
else {
    console.log("b is not a finite Number, it is either +Infinity or -Infinity");
}
if (isFinite(c_31)) {
    console.log(c_31);
}
else {
    console.log("c is not a finite Number, it is either +Infinity or -Infinity");
}
if (isNaN(d_31)) {
    console.log(d_31);
}
else {
    console.log("d is a real Number, not NaN");
}
if (isFinite(d_31)) {
    console.log(d_31);
}
else {
    console.log("d is not a finite Number, it is either +Infinity or -Infinity");
}
