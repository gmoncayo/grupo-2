console.log("-----------------------------Ejercicio Página 47---------------------------");
var nombre = 'Bob';
console.log('Hello ' + nombre + '!'); // The old way to do this, cumbersome and error prone
console.log("Hello " + nombre + "!"); // Template Literals, shorter, more succinct, and less
var otherName = 'Mary';
var thirdName = 'Jim';
console.log('Hello ' + otherName + ', how is ' + thirdName + '?'); // Can get very messy
console.log("Hello " + otherName + ", how is " + thirdName + "?");
