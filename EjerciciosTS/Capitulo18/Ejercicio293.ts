var textField = document.getElementsByTagName('input');
var isListening:boolean = true;
function changeMessage(e: any){
    if(isListening){
        isListening = !isListening;
        document.removeEventListener('click', changeMessage);
        textField[0].value = 'eventListener = ' + isListening;
        }
    }
    function setupDoc(e:any){
        textField[0].value = 'eventListener = true';
        document.addEventListener('click', changeMessage);
    }
    document.addEventListener('DOMContentLoaded', setupDoc);