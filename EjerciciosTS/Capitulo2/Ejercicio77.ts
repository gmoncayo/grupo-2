
    console.log("-----------------------------Página 77---------------------------");

    console.log( typeof 0 ); // "number"
    console.log( typeof Infinity ); // "number"
    console.log( typeof NaN ); // "number"
    console.log( typeof 'Hi' ); // "string"
    console.log( typeof "Hi" ); // "string"
    console.log( typeof true ); // "boolean"
    console.log( typeof false ); // "boolean"
    console.log( typeof undefined ); // "undefined"
    console.log( typeof 'symbol' ); // "symbol" - (careful, only in ES6)
    console.log( typeof {} ); // "object"
    console.log( typeof [] ); // "object"
    console.log( typeof new Date() ); // "object"
    console.log( typeof /^$/ ); // "object"
    console.log( typeof null ); // "object"
    console.log( typeof new String() ); // "object"
    console.log( typeof new Number() ); // "object"
    console.log( typeof typeof {} ); // "string"
