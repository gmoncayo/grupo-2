console.log("-----------------------------Página 76---------------------------");
console.log(void (0)); // returns Undefined
var object = { one: 1, two: 2 };
//typeof object; - undefined
delete object.one; // returns true
console.log(void (delete object.one)); // returns Undefined
console.log(object.two); // returns the Number 2
console.log(void (object.two)); // returns Undefined
function addNumbers(a, b) {
    return a + b;
}
console.log(addNumbers(1, 2)); // returns 3
console.log(void (addNumbers(1, 2))); // returns Undefined
