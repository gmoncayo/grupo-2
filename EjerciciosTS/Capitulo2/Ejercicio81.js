"use strict";
console.log("-----------------------------Pagina 81---------------------------");
var myDate = new Date();
if (myDate instanceof Date) {
    console.log('myDate is a Date instance');
}
if (myDate instanceof Array) {
    console.log('myDate is an Array instance');
}
if (typeof myDate === 'symbol') {
    console.log('myDate is a Symbol instance');
}
if (myDate instanceof Object) {
    console.log('myDate is an Object instance');
}
var mySymbol = Symbol();
if (typeof mySymbol === 'string') {
    console.log('mySymbol is a Date instance');
}
if (typeof mySymbol === 'function') {
    console.log('mySymbol is an Array instance');
}
if (typeof mySymbol === 'symbol') {
    console.log('mySymbol is a Symbol instance');
}
if (typeof mySymbol === 'object') {
    console.log('mySymbol is an Object instance');
}
var myArray = new Array();
if (myArray instanceof Date) {
    console.log('myArray is a Date instance');
}
if (myArray instanceof Array) {
    console.log('myArray is an Array instance');
}
if (myArray instanceof Symbol) {
    console.log('myArray is a Symbol instance');
}
if (myArray instanceof Object) {
    console.log('myArray is an Object instance');
}
var myObject = new Object();
if (myObject instanceof Date) {
    console.log('myObject is a Date instance');
}
if (myObject instanceof Array) {
    console.log('myObject is an Array instance');
}
if (typeof myObject === 'symbol') {
    console.log('myObject is a Symbol instance');
}
if (myObject instanceof Object) {
    console.log('myObject is an Object instance');
}
