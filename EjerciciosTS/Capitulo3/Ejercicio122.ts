console.log("-----------------------------Pagina 122---------------------------");
if ('Mike'.localeCompare('John') === -1) {
console.log('Mike comes before John');
} else if ('Mike'.localeCompare('John') === 1) {
console.log('John comes before Mike');
} else {
console.log('John and Mike are both in the same position');
}
if ('Jan'.localeCompare('John') === -1) {
console.log('Jan comes before John');
} else if ('Jan'.localeCompare('John') === 1) {
console.log('John comes before Jan');
} else {
console.log('John and Jan are both in the same position');
}
if ('a'.localeCompare('á') === -1) {
console.log('a comes before á');
} else if ('a'.localeCompare('á') === 1) {
console.log('á comes before a');
} else {
console.log('a and á are the same kind of letter');
}
if ('Michelle'.localeCompare('Michéllé') === -1) {
console.log('Michelle comes before Michéllé');
} else if ('Michelle'.localeCompare('Michéllé') === 1) {
console.log('Michéllé comes before Michelle');
} else {
console.log('Michéllé and Michelle are both in the same position');
}