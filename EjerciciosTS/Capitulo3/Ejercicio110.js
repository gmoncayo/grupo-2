console.log("-----------------------------Pagina 110---------------------------");
var abc = 'abc';
if (abc.startsWith('a')) {
    console.log('The string "abc" starts with the letter a');
}
else {
    console.log('The string "abc" does not start with the letter a');
}
if (abc.startsWith('b')) {
    console.log('The string "abc" starts with the letter b');
}
else {
    console.log('The string "abc" does not start with the letter b');
}
if (abc.startsWith('b', 2)) {
    console.log('The string "abc" have the letter a in second position');
}
else {
    console.log('The string "abc" does not end with the letter a');
}
if (abc.endsWith('c')) {
    console.log('The string "abc" ends with the letter c');
}
else {
    console.log('The string "abc" does not end with the letter c');
}
