var CheckArrayInstance = /** @class */ (function () {
    function CheckArrayInstance() {
    }
    CheckArrayInstance[Symbol.hasInstance] = function (instance) {
        return Array.isArray(instance);
    };
    return CheckArrayInstance;
}());
var myArray;
console.log(myArray instanceof CheckArrayInstance); //returns true
