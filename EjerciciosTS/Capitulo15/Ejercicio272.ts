class CheckArrayInstance {
    static [Symbol.hasInstance](instance){
        return Array.isArray(instance)
        }
}
var myArray:Array<string>;
console.log(myArray instanceof CheckArrayInstance); //returns true