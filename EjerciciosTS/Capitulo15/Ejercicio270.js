var _a, _b;
var helloSymbol = Symbol('Hello World Function');
var myObj = (_a = {},
    _a[helloSymbol] = function () {
        return 'Hello World';
    },
    _a);
console.log(myObj[helloSymbol.toString()]());
var iterableObj = (_b = {},
    _b[Symbol.iterator] = function () {
        var dataArray = ['this', 'that', 'other'];
        var currentIndex = 0;
        return {
            next: function () {
                if (currentIndex < dataArray.length) {
                    return { value: dataArray[currentIndex++] };
                }
                else {
                    return { done: true };
                }
            }
        };
    },
    _b);
for (var _i = 0, iterableObj_1 = iterableObj; _i < iterableObj_1.length; _i++) {
    var x = iterableObj_1[_i];
    console.log(x); //returns this, that, other
}
