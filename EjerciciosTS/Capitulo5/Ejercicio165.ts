console.log("-----------------------------Pagina 165---------------------------");
function hex2rgb(hex:number) {
return {
r: hex >> 16,
g: hex >> 8 & 255,
b: hex & 255,
};
}
function rgb2hex(r:number, g:number, b:number) {
return ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}
console.log(hex2rgb(0xFFFFFF));
console.log(hex2rgb(0xFF69B4));
console.log(hex2rgb(0xDAA520));
console.log(rgb2hex(255, 255, 255));
console.log(rgb2hex(255, 105, 180));
console.log(rgb2hex(218, 165, 32));