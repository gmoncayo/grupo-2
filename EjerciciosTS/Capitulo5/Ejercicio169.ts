console.log("-----------------------------Pagina 169---------------------------");
var execute = 1 << 0, // 001 or 1
write = 1 << 1, // 010 or 2
read = 1 << 2; // 100 or 4
function removePermission(permission, flag) {
return permission & ~flag;
}
function addPermission(permission, flag) {
return permission | flag;
}
function togglePermission (permission:number, flag:number) {
return permission ^ flag;
}
function permissionToString(permission) {
var stringPermission:string = '';
stringPermission += (permission & read) ? 'r' : '-';
stringPermission += (permission & write) ? 'w' : '-';
stringPermission += (permission & execute) ? 'x' : '-';
return stringPermission;
}
var writeExecute = write | execute, // 011 or 3
readExecute = read | execute, // 101 or 5
readWrite = read | write, // 110 or 6
full = read | write | execute; // 111 or 7
console.log( permissionToString(1) ); // —x
console.log( permissionToString(2) ); // -wconsole.
console.log( permissionToString(3) ); // -wx
console.log( permissionToString(4) ); // r—
console.log( permissionToString(5) ); // r-x
console.log( permissionToString(6) ); // rw
console.log( permissionToString(7) ); // rwx
console.log( permissionToString( removePermission(7, read) ) ); // -wx
console.log( permissionToString( removePermission(7, write) ) ); // r-x
console.log( permissionToString( removePermission(7, execute) ) ); // rwconsole.
console.log( permissionToString( addPermission(6, execute) ) ); // rwx
console.log( permissionToString( removePermission(7, execute) ) ); // rwconsole.
console.log( permissionToString( togglePermission(6, execute) ) ); // rwx
console.log( permissionToString( togglePermission(7, execute) ) ); // rw-