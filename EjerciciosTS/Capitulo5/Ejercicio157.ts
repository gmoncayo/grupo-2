console.log("-----------------------------Pagina 157---------------------------");
2 >>> 0; // 2, the same as ⌊2 ÷ 20⌋or ⌊2⌋. This returns a coerced 32-bit Unsigned Integer
2 >>> 1; // 1, the same as ⌊2 ÷ 21⌋ or ⌊2 ÷ 2⌋
2 >>> 4; // 0, the same as ⌊2 ÷ 24⌋ or ⌊2 ÷ 16⌋
16 >>> 4; // 1, the same as ⌊16 ÷ 24⌋ or ⌊16 ÷ 16⌋
var myNum:number = 1234, pow:number = 16;
myNum >>> pow; // 0, the same as ⌊1234 ÷ 216⌋ or ⌊1234 ÷ 65536⌋
console.log(myNum+', '+pow);