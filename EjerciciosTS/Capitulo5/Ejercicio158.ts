console.log("-----------------------------Pagina 158---------------------------");
~0; // -1, the same as ⌊ -(0 + 1) ⌋
~1; // -2, the same as ⌊ -(1 + 1) ⌋
~2; // -3, the same as ⌊ -(2 + 1) ⌋
~3; // -4, the same as ⌊ -(3 + 1) ⌋
~4; // -5, the same as ⌊ -(4 + 1) ⌋
~5; // -6, the same as ⌊ -(5 + 1) ⌋
~6; // -7, the same as ⌊ -(6 + 1) ⌋
~7; // -8, the same as ⌊ -(7 + 1) ⌋
~8; // -9, the same as ⌊ -(8 + 1) ⌋
~2147483648; // 2147483647, reaches the integer overflow limit
~-1; // 0, the same as ⌊ -(-1 + 1) ⌋
~-27; // 26, the same as ⌊ -(-27 + 1) ⌋
~-2423; // 2422, the same as ⌊ -(-2423 + 1) ⌋
~-2147483648; // 2147483647, the same as ⌊ -(-2147483648 + 1)
console.log(~0);
console.log(~1);
console.log(~2);
console.log(~3);
console.log(~4);
console.log(~5);
console.log(~6);