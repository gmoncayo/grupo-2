"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Robot = /** @class */ (function () {
    function Robot() {
        this.type;
    }
    return Robot;
}());
var BendingUnit = /** @class */ (function (_super) {
    __extends(BendingUnit, _super);
    function BendingUnit() {
        var _this = _super.call(this) || this;
        _this.name;
        _this.occupation = 'Industrial Robot';
        _this.origin = 'Tijuana, Mexico';
        return _this;
    }
    return BendingUnit;
}(Robot));
var AstromechDroid = /** @class */ (function (_super) {
    __extends(AstromechDroid, _super);
    function AstromechDroid() {
        var _this = _super.call(this) || this;
        _this.name;
        return _this;
    }
    return AstromechDroid;
}(Robot));
var bender = new BendingUnit();
bender.type = 'Bending Unit 22';
bender.name = 'Bender Bending Rodriguez';
console.log(bender.type); //returns Bending Unit 22
console.log(bender.name); //returns Bender Bending Rodriguez
var r2d2 = new AstromechDroid();
r2d2.type = 'Astromech Droid';
r2d2.name = 'R2-D2';
console.log(r2d2.type); //returns Astromech Droid
console.log(r2d2.name); //returns R2-D2
