"use strict";
var Cookies = /** @class */ (function () {
    function Cookies() {
        this._typeOfCookie;
    }
    Object.defineProperty(Cookies.prototype, "cookieType", {
        get: function () {
            return this._typeOfCookie;
        },
        set: function (typeOfCookie) {
            this._typeOfCookie = typeOfCookie;
        },
        enumerable: false,
        configurable: true
    });
    return Cookies;
}());
var myCookie = new Cookies();
myCookie.cookieType = "Chocolate Chip";
console.log(myCookie.cookieType); //returns Chocolate Chip;
console.log(myCookie._typeOfCookie);
