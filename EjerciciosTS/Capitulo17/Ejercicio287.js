var Human1 = /** @class */ (function () {
    function Human1() {
    }
    Human1.hasLegs = function () {
        return 'Person has legs';
    };
    Human1.hasAmrs = function () {
        return 'Person has arms';
    };
    return Human1;
}());
console.log(Human1.hasLegs()); //returns Person has legs
console.log(Human1.hasAmrs()); //returns Person has arms
