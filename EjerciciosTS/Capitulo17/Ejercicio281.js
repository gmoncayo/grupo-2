var Human = (function Human(name) {
    this.name = name;
});
Human.prototype.sayGoodNight = function () {
    return 'Say Goodnight ' + this.name;
};
var george = ('Gracie');
console.log(george.sayGoodNight());
//ECMAScript 6 class
var Greeting = /** @class */ (function () {
    function Greeting(name) {
        this.name = name;
    }
    Greeting.prototype.sayHello = function () {
        return 'Hellooo ' + this.name;
    };
    return Greeting;
}());
var yakko = new Greeting('Nurse!');
console.log(yakko.sayHello());
