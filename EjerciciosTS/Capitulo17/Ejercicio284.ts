class Cookies{
    _typeOfCookie:string
    constructor(){
        this._typeOfCookie;
    }
    set cookieType(typeOfCookie:string){
    this._typeOfCookie = typeOfCookie;
    }
    get cookieType(){
    return this._typeOfCookie;
    }
}
var myCookie = new Cookies();
myCookie.cookieType = "Chocolate Chip";
console.log(myCookie.cookieType); //returns Chocolate Chip;
console.log(myCookie._typeOfCookie);