console.log("-----------------------------Página 174---------------------------");
var now = new Date(); //returns todays date and time
console.log(now.getDate()); //returns the day of the month from 1 to 31
console.log(now.getDay()); //returns the day of the week its zero based like an array 0 - 6;
console.log(now.getFullYear()); //returns the current year
console.log(now.getHours()); //returns hours from 0-23
console.log(now.getMonth()); //return month from 0-11
//time information
console.log(now.getSeconds()); //returns seconds from 0-59
console.log(now.getTime()); //returns the amount of milliseconds since the first of January 1970
