"use strict";
console.log("-----------------------------Página 178---------------------------");
//Want to know what the offset between UTC time and my local time
var currentDate = new Date();
var offSet = currentDate.getTimezoneOffset(); // 60; converts minutes to hours
console.log(offSet);
//check if we are on daylight savings time
var today = new Date();
function isDST() {
    var jan = new Date(today.getFullYear(), 0, 1);
    var jul = new Date(today.getFullYear(), 6, 1);
    return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
}
if (isDST() != today.getTimezoneOffset()) {
    console.log('On DST');
}
else {
    console.log('Not On DST');
}
