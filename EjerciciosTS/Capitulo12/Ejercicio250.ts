
    console.log("-----------------------------Página 250---------------------------");
    function showSpread(one, two, three, four): void{
    console.log(one);
    console.log(two);
    console.log(three);
    console.log(four);
    }
    let myArray: number[] = [1,2,3,4];
    //showSpread(myArray[0], myArray[1], myArray[2], myArray[3]) //without using spread
    //showSpread(...myArray); //using the spread syntax
    let dayInfo: number[] = [1975, 7, 19];
    let dateObj: Date = new Date(dayInfo[0], dayInfo[1], dayInfo[2]);
    console.log(dateObj); //returns Tue Aug 19 1975 00:00:00 GMT-0400 (EDT)
    let numArray2: number[] = [ 2, 3, 4, 5, 6, 7]
    let numArray3: number[] = [1, ...numArray2, 8, 9, 10];
    console.log(numArray3); //returns 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
    let part1: number[] = [1,2,3];
    let part2: number[] = [4,5,6];    
    let part3: number[] = [...part1, ...part2];
    console.log(part3); //returns 1,2,3,4,5,6