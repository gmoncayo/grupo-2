
    console.log("-----------------------------Página 249---------------------------");
    function showSpread1(one, two, three, four): void{
        console.log(one);
        console.log(two);
        console.log(three);
        console.log(four);
    }
    var myArray1: number[] = [1,2,3,4];
    //showSpread(myArray[0], myArray[1], myArray[2], myArray[3]) //without using spread
    //showSpread(...myArray); //using the spread syntax
    var dayInfo1: number[] = [1975, 7, 19];
    var dateObj1 = new Date(dayInfo1[0], dayInfo1[1], dayInfo1[2]);
    console.log(dateObj); //returns Tue Aug 19 1975 00:00:00 GMT-0400 (EDT)
    var numArray21: number[] = [ 2, 3, 4, 5, 6, 7]
    var numArray1: number[] = [1, ...numArray2, 8, 9, 10];
    console.log(numArray); //returns 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
   
