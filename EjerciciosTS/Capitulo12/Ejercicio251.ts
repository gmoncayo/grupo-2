
    console.log("-----------------------------Página 251---------------------------");
    function showSpread251(one, two, three, four): void{
    console.log(one);
    console.log(two);
    console.log(three);
    console.log(four);
    }
    var myArray5: number[] = [1,2,3,4];
    //showSpread(myArray[0], myArray[1], myArray[2], myArray[3]) //without using spread
    //showSpread(...myArray); //using the spread syntax
    var dayInfo5 : number[]= [1975, 7, 19];
    var dateObj5: Date = new Date(dayInfo5[0], dayInfo5[1], dayInfo5[2] );
    console.log(dateObj); //returns Tue Aug 19 1975 00:00:00 GMT-0400 (EDT)
    var numArray25 : number[]= [ 2, 3, 4, 5, 6, 7]
    var numArray5 : number[]= [1, ...numArray2, 8, 9, 10];
    console.log(numArray5); //returns 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
    var part15 : number[]= [1,2,3];
    var part25 : number[]= [4,5,6];
    var part35 : number[]= [...part15, ...part25];
    console.log(part35); //returns 1,2,3,4,5,6
